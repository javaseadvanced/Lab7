# Exercise7
Регулярные выражения

#Для запуска проекта скопируйте указанный ниже код
#Вставьте с ПОЛНОЙ заменой в файл tasks.json
#Расположение файла tasks.json: Explorer => первая папка ".theia" => tasks.json

# Запуск c пакетами ( успешно )

{
    "tasks": [
        {
            "type": "che",
            "label": "PhoneCheck build and run",
            "command": "javac -d /projects/Lab7/Regex/bin -sourcepath /projects/Lab7/Regex/src /projects/Lab7/Regex/src/ru/qdts/p1/PhoneCheck.java && java -classpath /projects/Lab7/Regex/bin ru.qdts.p1.PhoneCheck",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Lab7/Regex/src",
                "component": "maven"
            }
        },
        {
            "type": "che",
            "label": "EmailCheck build and run",
            "command": "javac -d /projects/Lab7/Regex/bin -sourcepath /projects/Lab7/Regex/src /projects/Lab7/Regex/src/ru/qdts/p1/EmailCheck.java && java -classpath /projects/Lab7/Regex/bin ru.qdts.p1.EmailCheck",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Lab7/Regex/src",
                "component": "maven"
            }
        }
    ]
}
