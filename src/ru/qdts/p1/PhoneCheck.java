package ru.qdts.p1;

import java.util.List;

public class PhoneCheck {

	public static void main(String[] args) {
		/*
		 * Ваша задача состоит в следующем:
		 * 1) Написать функцию (readStrings), которая считывает построчно содержимое заданного аргументом файла из пакета ru.qdts.p1 и возвращает List<String>.
		 * 2) Написать функцию (checkStrings), которая принимает в качестве первого аргумента List<String> со строками для анализа, 
		 *			а в качестве второго - строку с регулярным выражением для анализа.
		 *			Функция должна вывести результат в консоль: по каждой строке - соответствует ли она регулярному выражению.
		 *			Также, мы должны увидеть в консоли статистику: сколько строк соответствуют регулярному выражению, а сколько - нет.
		 * 3) Написать такое регулярное выражение, которое позволит нам выполнять проверку телефона с учетом следующих условий:
		 * 			а) Телефон начинается или с 8, или с 7
		 * 			б) Если он начинается на 7, то в начале строки может стоять + (не обязательно)
		 * 			в) Вторая цифра - всегда 9
		 * 			г) Всего в номере 11 цифр
		 * 			д) Могут использоваться дефисы после 1,4,7,9 цифр
		 * 			е) Прочие символы недопустимы
		 * 4) Выполнить проверку строк из файла InvalidPhones с использованием созданного регулярного выражения: ни один из телефонов не должен пройти проверку
		 * 5) Выполнить проверку строк из файла ValidPhones с использованием созданного регулярного выражения: все телефоны должны пройти проверку
		 */
		
		List<String> InvalidPhones = readStrings("InvalidPhones");
		List<String> ValidPhones = readStrings("ValidPhones");
		
		String phoneRegex = "";
		checkStrings(InvalidPhones, phoneRegex);
		checkStrings(ValidPhones, phoneRegex);
	}
	
	public static List<String> readStrings(String fileName) {
		List<String> lines = null;	
		return lines;
	}
	
	public static void checkStrings(List<String> lines, String regex) {
	}
}
