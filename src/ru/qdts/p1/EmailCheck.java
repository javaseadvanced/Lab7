import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class EmailCheck {

    public static void main(String[] args) throws IOException {
        /*
         * Ваша задача состоит в следующем:
         * 1) Написать функцию (readStrings), которая считывает построчно содержимое заданного аргументом файла из пакета ru.qdts.p1 и возвращает List<String>.
         * 			* можно использовать функцию, написанную для предыдущей части задания
         * 2) Написать функцию (checkStrings), которая принимает в качестве первого аргумента List<String> со строками для анализа,
         *			а в качестве второго - строку с регулярным выражением для анализа.
         *			Функция должна вывести результат в консоль: по каждой строке - соответствует ли она регулярному выражению.
         *			Также, мы должны увидеть в консоли статистику: сколько строк соответствуют регулярному выражению, а сколько - нет.
         *			* можно использовать функцию, написанную для предыдущей части задания
         * 3) Написать такое регулярное выражение, которое позволит нам выполнять проверку email (username@domain1.domain0) с учетом следующих условий:
         * 			а) domain0: com, или edu, или org, или net, или любые две латинские буквы
         * 			б) domain1: здесь можно использовать латинские буквы, цифры, дефисы; дефиса не может быть в начале и в конце; максимальная длина 63 символа
         * 			в) username: здесь можно использовать латинские буквы, цифры, некоторые спецсимволы (- _ $ #), и точку, которая не может идти в начале и в конце (также не могут идти подряд 2 точки).
         * 			г) описанные выше условия не отражают спецификации для почтовых адресов, а приведены лишь для примера: при желании вы можете адаптировать эти условия или добавить новые
         * 4) Выполнить проверку строк из файла InvalidEmails с использованием созданного регулярного выражения: ни один из email не должен пройти проверку
         * 5) Выполнить проверку строк из файла ValidEmails с использованием созданного регулярного выражения: все email должны пройти проверку
         */

        List<String> InvalidEmails = readStrings("InvalidEmails");
        List<String> ValidEmails = readStrings("ValidEmails");
        String emailRegex = " ";

        printName("InvalidEmails");
        checkStrings(InvalidEmails, emailRegex);
        printName("ValidEmails");
        checkStrings(ValidEmails, emailRegex);

        typeCheck(emailRegex);

    }

    public static List<String> readStrings(String fileName) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(fileName));
        return lines;
    }

    public static void checkStrings(List<String> lines, String regex) {
        int cnt = 1;
        int countertrue = 0;
        int counterfalse = 0;
        for (String line : lines) {
            Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
            Matcher m = pattern.matcher(line);
            if (m.matches()) {
                System.out.println(cnt + ")" + line + " - правильный Email ");
                countertrue++;
                cnt++;
            } else {
                System.out.println(cnt + ")" + line + " - неправильный Email ");
                counterfalse++;
                cnt++;
            }
        }
        System.out.println("Всего строк в файле " + (countertrue + counterfalse) + "\nИз них Соответствий: " + countertrue + ". Несоответствий: " + counterfalse);
        System.out.println("Конец проверки \n");
    }

    public static void printName(String fileName) {
        System.out.println("Проверка для файла " + fileName);
    }
    public static void typeCheck(String regex){
        System.out.println("Проверка на правильность ввода Email адреса из консоли\nВведите Email адрес");
        List<String>typeString = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        typeString.add(sc.nextLine());
        sc.close();
        checkStrings(typeString, regex);
    }


}
